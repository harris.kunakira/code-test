import Link from "next/link";

export default function Home() {
  return (
    <ol>
      <li>
        <Link href="/tests/advance-search">Advance Search</Link>
      </li>
      <li>
        <Link href="/tests/master-mind">Master Mind</Link>
      </li>
      <li>
        <Link href="/tests/array-sort">Array Sorting Probability</Link>
      </li>
      <li>
        <Link href="/tests/decimal">Find 10,000th decimal place value</Link>
      </li>
    </ol>
  );
}
