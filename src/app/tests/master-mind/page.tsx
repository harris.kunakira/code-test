"use client";

import { useEffect, useState } from "react";

const random = () =>
  Array.from({ length: 4 }, () => Math.floor(Math.random() * 10));

export default function MasterMind() {
  const [randomNumber, setRandomNumber] = useState(random);
  const [guessed, setGuessed] = useState("");
  const [correctGuess, setCorrectGuess] = useState(0);
  const [correctPosition, setCorrectPosition] = useState(0);
  const [isWon, setIsWon] = useState(false);
  const [resultText, setResultText] = useState("Submit");

  const handleInput = (event: React.ChangeEvent) => {
    const { value } = event.target as HTMLInputElement;
    const guessArray = value.split(",");
    if (guessArray.length > 4) {
      event.preventDefault();
      return;
    }
    setResultText("");
    setGuessed(value);
    setCorrectGuess(
      guessArray.filter((item) => randomNumber.includes(parseInt(item))).length
    );
    setCorrectPosition(
      guessArray.filter((item, index) => randomNumber[index] === parseInt(item))
        .length
    );
  };

  useEffect(() => {
    if (correctPosition === 4) {
      setIsWon(true);
      setResultText("You Won!");
    }
  }, [correctPosition]);

  const handleTryAgain = () => {
    setGuessed("");
    setCorrectGuess(0);
    setCorrectPosition(0);
    setIsWon(false);
    setResultText("");
    setRandomNumber(random);
  };

  return (
    <div>
      <h1>Master Mind</h1>
      <div>
        <label>Guess numbers:</label>
        <input
          type="text"
          placeholder="Input 4 numbers eg. 1,2,3,4"
          value={guessed}
          onChange={handleInput}
        />

        <br />
        <br />
        <pre>Answer: {JSON.stringify(randomNumber)}</pre>
        <div>
          <strong>Correct Guess: {correctGuess}</strong>
          <br />
          <strong>Correct Position: {correctPosition}</strong>
          {isWon && (
            <div>
              <h2>{resultText}</h2>
              <br />
              {isWon && (
                <button onClick={handleTryAgain}>You Won! Play Again?</button>
              )}
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
