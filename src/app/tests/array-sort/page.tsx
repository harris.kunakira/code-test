"use client";

import { useEffect, useState } from "react";

const processInput = (inputArray: number[]) => {
  let result = [];

  const sorting = (a: number, b: number) => {
    const temp = inputArray[a];
    inputArray[a] = inputArray[b];
    inputArray[b] = temp;
  };

  const process = (numberOfItem: number, array: number[]) => {
    if (numberOfItem === 1) {
      result.push([...array]);
      return;
    }

    for (let i = 0; i < numberOfItem; i++) {
      process(numberOfItem - 1, array);
      sorting(numberOfItem % 2 ? 0 : i, numberOfItem - 1);
    }
  };

  process(inputArray.length, inputArray);
  return result;
};

export default function ArraySort() {
  const [inputValue, setInputValue] = useState("");
  const [sortingCombination, setSortingCombination] = useState("");

  const handleInput = (event: React.ChangeEvent) => {
    const { value } = event.target as HTMLInputElement;
    setInputValue(value);
    if (!isNaN(parseInt(value.at(-1)))) {
      const inputArray = value.split(",").map((item) => parseInt(item));
      const allPossibleCombination = processInput(inputArray);
      setSortingCombination(JSON.stringify(allPossibleCombination));
    }
  };

  useEffect(() => {
    inputValue.length === 0 && setSortingCombination("");
  }, [inputValue]);

  return (
    <div>
      <h1>Sorting Probability</h1>
      <div>
        <label>Input any number:</label>
        <input
          type="text"
          placeholder="1,2,3,4..."
          value={inputValue}
          onChange={handleInput}
        />
        <br />
        <br />
        <div>
          <strong>Sorting Combination: {sortingCombination}</strong>
        </div>
      </div>
    </div>
  );
}
