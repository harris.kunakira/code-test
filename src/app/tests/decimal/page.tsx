"use client";

import { useState } from "react";
import { Decimal } from "decimal.js";

export default function DecimalPlace() {
  const [inputValue, setInputValue] = useState("");
  const [inputDecimalPlace, setInputDecimalPlace] = useState(0);
  const [inputArray, setInputArray] = useState([]);
  const [result, setResult] = useState("");

  const handleInput = (event: React.ChangeEvent) => {
    const { value } = event.target as HTMLInputElement;
    const valueArray = value.split("/");
    if (valueArray.length > 2) {
      event.preventDefault();
      return;
    }
    setInputValue(value);
    setInputArray(valueArray);
    setResult("");
  };

  const handleInputDecimalPlace = (event: React.ChangeEvent) => {
    const { value } = event.target as HTMLInputElement;
    setInputDecimalPlace(parseInt(value));
    setResult("");
  };

  const handleSubmit = (event: React.MouseEvent) => {
    Decimal.set({ precision: inputDecimalPlace });
    const number1 = new Decimal(inputArray[0]);
    const number2 = new Decimal(inputArray[1]);
    const decimal = number1.dividedBy(number2);
    setResult(decimal.toString().charAt(inputDecimalPlace - 1));
  };

  return (
    <div>
      <h1>10,000th Decimal</h1>
      <div>
        <label>Input 2 numbers and enter:</label>
        <input
          type="text"
          placeholder="eg. 1/2"
          value={inputValue}
          onChange={handleInput}
        />
        <label>What decimal place do you want to know:</label>
        <input
          type="number"
          placeholder="Decimal Place"
          value={inputDecimalPlace}
          onChange={handleInputDecimalPlace}
        />
        <button onClick={handleSubmit}>Submit</button>
        <br />
        <br />
        <div>
          {result && (
            <strong>
              {inputDecimalPlace} decimal place of {inputArray[0]}/
              {inputArray[1]}: {result}
            </strong>
          )}
        </div>
      </div>
    </div>
  );
}
