import Link from "next/link";

export const metadata = {
  title: "Code Test",
  description: "",
};

export default function TestLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <main>
      <Link href="/">Back</Link>
      {children}
    </main>
  );
}
