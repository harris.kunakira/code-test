"use client";

import React, { useState } from "react";
import testData from "@/assets/data/test.json";
import Link from "next/link";

const data = testData.data as any;

export default function AdvanceSearch() {
  const dropdownOption1 = [];
  const dropdownOption2 = {};

  Object.keys(data).forEach((item: string) => {
    const nestedItems = [];
    Object.keys(data[item]).forEach((childItem) => {
      nestedItems.push({
        label: childItem,
        value: childItem,
      });
      Object.assign(dropdownOption2, {
        [childItem]: data[item][childItem].map((item) => ({
          label: item,
          value: item,
        })),
      });
    });

    dropdownOption1.push({
      label: item,
      value: item,
      children: nestedItems,
    });
  });

  const [isDropdown1Active, setIsDropdown1Active] = useState(false);
  const [isDropdown2Active, setIsDropdown2Active] = useState(false);
  const [selectedItems, setSelectedItems] = useState([]);
  const [selectedChildItems, setSelectedChildItems] = useState([]);
  const [displayedDropdown2Items, setDisplayedDropdown2Items] = useState([]);

  const toggleDropdown1 = () => setIsDropdown1Active(!isDropdown1Active);
  const toggleDropdown2 = () => setIsDropdown2Active(!isDropdown2Active);

  const handleDropdown1Level1 = (optionVal: string) => {
    if (!selectedItems.includes(optionVal)) {
      setSelectedItems([...selectedItems, optionVal]);
    } else {
      setSelectedItems(selectedItems.filter((item) => item !== optionVal));
    }
  };

  const handleDropdown1Level2 = (optionVal) => {
    if (!selectedChildItems.includes(optionVal)) {
      setSelectedChildItems([...selectedChildItems, optionVal]);
      setDisplayedDropdown2Items([
        ...displayedDropdown2Items,
        dropdownOption2[optionVal],
      ]);
    } else {
      setSelectedChildItems(
        selectedChildItems.filter((item) => item !== optionVal)
      );
      setDisplayedDropdown2Items(
        displayedDropdown2Items.filter(
          (item) =>
            JSON.stringify(item) !== JSON.stringify(dropdownOption2[optionVal])
        )
      );
    }
  };

  return (
    <div>
      <h1>Advance Search</h1>
      <div className="searchWrapper">
        <div className="inputWrapper">
          <button className="dropdown-toggle" onClick={toggleDropdown1}>
            Select region and province
          </button>
          {isDropdown1Active && (
            <div className="dropdown-menu">
              {dropdownOption1.map((option) => (
                <div key={option.value} className="dropdown-item">
                  <div
                    className="option"
                    onChange={() => handleDropdown1Level1(option.value)}
                  >
                    <input
                      type="checkbox"
                      checked={selectedItems.includes(option.value)}
                      readOnly
                    />
                    <label>{option.label}</label>
                  </div>
                  {selectedItems.includes(option.value) && (
                    <div className="nested-options">
                      {option.children.map((childOption) => (
                        <div
                          key={childOption.value}
                          className="nested-option"
                          onChange={() =>
                            handleDropdown1Level2(childOption.value)
                          }
                        >
                          <input
                            type="checkbox"
                            checked={selectedChildItems.includes(
                              childOption.value
                            )}
                            readOnly
                          />
                          <label>{childOption.label}</label>
                        </div>
                      ))}
                    </div>
                  )}
                </div>
              ))}
            </div>
          )}
        </div>
        <div className="inputWrapper">
          <button className="dropdown-toggle" onClick={toggleDropdown2}>
            Select Nationality
          </button>
          <div className="dropdown-menu">
            {displayedDropdown2Items.map((item) =>
              item.map((option) => (
                <div key={option.value} className="dropdown-item">
                  <div className="option">
                    <input type="checkbox" />
                    <label>{option.label}</label>
                  </div>
                </div>
              ))
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
